package practice.first;

public class PimitiveTypesLoopsUtil {
	/*
	public static byte newByte;
	public static short newShort;
	public static int newInt;
	public static long newLong;
	public static char newChar;
	public static float newFloat;
	public static double newDouble;
	public static boolean newBool;
	*/
	public static void printPrimitivTypes(){
		
		byte newByte = 1;
		short newShort = 1;
		int newInt = 1;
		long newLong = 1;
		char newChar = '1';
		float newFloat = 1.1f;
		double newDouble = 1.1;
		boolean newBool = true;
		
		System.out.println("" + newByte);
		System.out.println("" + newShort);
		System.out.println("" + newInt);
		System.out.println("" + newLong);
		System.out.println("" + newChar);
		System.out.println("" + newFloat);
		System.out.println("" + newDouble);
		System.out.println("" + newBool);
	}
		
	public static void printFloatTypes(){
		//float fl = 1.;
		float flo = 1;
		float flt = 0x1;
		float fla = 0b1;
		//float floo = 1.0e1;
		float fll = 01;
	}
	
	public static void printShortInitialize(){
		byte b = 1 ;
		float f = 1.1f;
		double d = 1.2;
		int i = 1;
		short sh1 = 1;
		
		short s = 1 + 1 ;
		short sh = (short) (1.1 + 1);
		short sho = (short) (f + i);
		short shor = (short) (b + sh1);
		short shrt = (short) (f + d);	
	}
	
	public static void printPifagorMethod(int katFirs, int katSec, int gip){
		int katetRes = katFirs*katFirs + katSec*katSec;
		int gipRes = gip*gip;
		String mainRes = (katetRes == gipRes)?"Yes":"No";
		System.out.print(mainRes);
		}
	
	public static void printSumIntegerNums(){
		int temp = 0;
		int a = 0;
		while(a < 20){
			temp += a;
			a++;
		}System.out.print(temp);
	}
	
	public static void printSumEvenNums(int a){
		int temp = 0;
		while(a < 20){
			if(a % 2 == 0){
				temp += a;
				a++;
			}else{
				a++;
			}
		}System.out.print(temp);
	}
	
	public static void printSimpleNums(){
		
		int temp = 0;
		m:for(int a = 2; a <= 20; a++){
			for(int b = 2; b < a; b++){
				if(a % b == 0){
					continue m;
				}
			}
			temp += a;
		}
		System.out.println(temp);
	}
	
	public static void printSumOfThree(int a, int b, int c){
		
		int temp = 0;
		while(a + b != c){
			temp = a;
			a = b;
			b = c;
			c = temp;
		} System.out.println("true");
	}
	
	public static void printAverage(int firstNum, int lastNum){
		
		int temp = 0;
		int count = 0;
		int average;
		for(; firstNum > lastNum; firstNum--){
			temp += firstNum;
			count++;
		}
		average = temp/count;
		System.out.print(average);
	}
	
 	public static void Credit(double amountOfCredit, double interestRate, int loanTerm){
 		
 		double loanPayment;
 		double interestCharges;
 		double sumOfInterestCharges = 0;
 		int monthCount = 1;
 		
 		for(; monthCount <= loanTerm; monthCount++){
 			loanPayment = amountOfCredit / loanTerm;
 			interestCharges = loanPayment * (interestRate / 100);
 			sumOfInterestCharges = interestCharges * loanTerm;
 			System.out.println("������ �� " + monthCount + " ����� - " + loanPayment);
 			System.out.println("���������� ������ �� " + monthCount + "����� - " + interestCharges);
 		}System.out.println("����� ����������� ��������� - " + sumOfInterestCharges);
		
 	}
}

