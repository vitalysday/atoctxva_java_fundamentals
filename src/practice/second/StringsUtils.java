package practice.second;

public class StringsUtils {
	
	public static String stringWithThreeChars(String str){
		
		if(str.length() % 2 != 0 && str.length() > 3){
			int index = str.length() / 2;
			String sub = str.substring(index - 1, index + 2);
			return sub;
		}
		return str;
	} 
		
} 
	



