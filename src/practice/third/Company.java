package practice.third;

import java.util.ArrayList;
import java.util.List;

public class Company{
	
	private String nameOfCompany;
	
	public Company(String nameOfCompany) {
		super();
		this.nameOfCompany = nameOfCompany;
	}
	
	List<Employee> list = new ArrayList<>();
	
	public void addEmployee(Employee emp){
		list.add(emp);
	}
	public double totalSalary(Employee emp){
		double tS = emp.calcSalary();
		return tS;
		
	}
}
