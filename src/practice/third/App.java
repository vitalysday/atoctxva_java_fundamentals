package practice.third;

public class App {

	public static void main(String[] args) {
		
		Company cm = new Company("DC");
		
		cm.addEmployee(new Boss("Bob", "Bober", 2400));
		cm.addEmployee(new HourlyWorker("John", "Gamp", 30, 8, 8));
	
		System.out.println();

	}

}
