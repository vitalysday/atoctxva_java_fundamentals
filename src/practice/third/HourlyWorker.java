package practice.third;

public class HourlyWorker extends Employee {
	
	public HourlyWorker(String firstName, String lastName, double salaryPerHour, int hoursDay, int moreHours) {
		super(firstName, lastName);
		
	}

	private double salaryPerHour;
	static final double BONUS = 0.10;
	private int hoursDay;
	private int moreHours;

	public double getSalaryPerHour() {
		return salaryPerHour;
	}

	public void setSalaryPerHour(double salaryPerHour) {
		this.salaryPerHour = salaryPerHour;
	}

	public int getMoreHours() {
		return moreHours;
	}


	public void setMoreHours(int moreHours) {
		this.moreHours = moreHours;
	}


	public int getHoursDay() {
		return hoursDay;
	}


	public void setHoursDay(int hoursDay) {
		this.hoursDay = hoursDay;
	}
	
	@Override
	double calcSalary() {
 		double salaryWithBonus;
		double dailySalary = salaryPerHour * hoursDay;
		if(moreHours > hoursDay){
			int temp = moreHours - hoursDay;
			salaryWithBonus = temp * BONUS + dailySalary;
		}else{
			return dailySalary;
		}
		double res = (moreHours > hoursDay)?salaryWithBonus:dailySalary;
		return res;
	}
	
	

}
