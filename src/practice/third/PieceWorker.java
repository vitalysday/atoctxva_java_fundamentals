package practice.third;

public class PieceWorker extends Employee {

	public PieceWorker(String firstName, String lastName, double priceForDetal, int dailyMade) {
		super(firstName, lastName);
		
	}
	
	private double priceForDetal;
	private int dailyMade;

	@Override
	double calcSalary() {
		double dailySalary = priceForDetal * dailyMade;
		return dailySalary;
	}

	public double getPriceForDetal() {
		return priceForDetal;
	}

	public void setPriceForDetal(double priceForDetal) {
		this.priceForDetal = priceForDetal;
	}

	public int getDailyMade() {
		return dailyMade;
	}

	public void setDailyMade(int dailyMade) {
		this.dailyMade = dailyMade;
	}
	
}
