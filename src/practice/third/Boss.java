package practice.third;

public class Boss extends Employee {
	
	public Boss(String firstName, String lastName, double salaryPerWeek) {
		super(firstName, lastName);
		setSalaryPerWeek(salaryPerWeek);
	}

	private double salaryPerWeek;

	public double getSalaryPerWeek() {
		return salaryPerWeek;
	}

	public void setSalaryPerWeek(double salaryPerWeek) {
		this.salaryPerWeek = salaryPerWeek;
	}

	@Override
	double calcSalary() {
		return salaryPerWeek * 4;
	}
	

}
