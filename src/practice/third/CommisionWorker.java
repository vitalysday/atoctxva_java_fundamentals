package practice.third;

public class CommisionWorker extends Employee {

	public CommisionWorker(String firstName, String lastName, double salaryPerMounth, int  mounthSale) {
		super(firstName, lastName);
		
	}
	
	private double salaryPerMounth;
	private int mounthSale;
	static final double BONUSOFSALES = 0.10;

	@Override
	double calcSalary() {
		double totalSalary = salaryPerMounth + (BONUSOFSALES * mounthSale);
		return totalSalary;
	}

	public double getSalaryPerMounth() {
		return salaryPerMounth;
	}

	public void setSalaryPerMounth(double salaryPerMounth) {
		this.salaryPerMounth = salaryPerMounth;
	}

}
